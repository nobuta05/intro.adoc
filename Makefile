vpath %.html public
vpath %.pdf public
vpath %.adoc partials

.PHONY: all
all: index.html ex.png

index.html: index.adoc docinfo-revealjs.html ex.png
	bundle exec asciidoctor-revealjs $< -o public/$@

ex.png: ex.pdf
	pdftoppm -png public/$< > public/$@

ex.pdf: ex.adoc
	bundle exec asciidoctor-pdf -r asciidoctor-pdf-cjk -a scripts=cjk -a pdf-theme=default-with-fallback-font $< -o public/$@
